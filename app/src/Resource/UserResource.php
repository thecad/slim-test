<?php
namespace App\Resource;

use App\AbstractResource;

/**
 * Class Resource
 * @package app
 */

class UserResource extends AbstractResource {
	/**
	 * @param string|null $id
	 *
	 * @return array
	 */
	public function get($id = null) {
		if ($id === null) {
			$users = $this->entityManager->getRepository('App\Entity\User')->findAll();
			$users = array_map(
				function ($user) {
					return $user->getArrayCopy();
				},
				$users
			);
			return $users;
		} else {
		  $user = $this->entityManager->getRepository('App\Entity\User')->findOneBy(array('id' => $id));

			if ($user) {
				return $user->getArrayCopy();
			}
		}
		return false;
	}
}

