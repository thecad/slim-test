<?php
namespace App\Action;

use App\Resource\UserResource;

final class UserAction {
	
	private $userResource;

	public function __construct(UserResource $userResource) {
		$this->userResource = $userResource;
	}

	public function fetch($request, $response, $args) {
		$users = $this->userResource->get();
		return $response->withJSON($users);
	}

	public function fetchOne($request, $response, $args) {
		$user = $this->userResource->get($args['id']);
		
		if ($user) {
			return $response->withJSON($user);
		}
		return $response->withStatus(404, 'No user found with this id');
	}
}

