<?php
namespace App\Entity;

use app\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="users")
 */
class User {
	/**
	 * @ORM\Id
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

	/**
	 * @ORM\Column(type="string", length=64)
	 */
	protected $username;

	/**
	 * @ORM\Column(type="string", length=64)
	 */
	protected $password;

	/**
	 * Get Array copy of the object
	 *
	 * @return array
	 */
	public function getArrayCopy() {
		return get_object_vars($this);
	}

	/**
	 * Get User id
	 *
	 * @ORM\return integer
	 */
	public function getId() {
		return $this->id;
	}

	/**
	 * Get User name
	 *
	 * @ORM\return string
	 */
	public function getUserName() {
		return $this->username;
	}

	/**
	 * Get User password (ofcourse this is an example)
	 *
	 * @ORM\return string
	 */
	public function getPassword() {
		return $this->password;
	}
}
