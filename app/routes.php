<?php
// Routes

$app->get('/', App\Action\HomeAction::class)
->setName('homepage');

$app->get('/api/users', 'App\Action\UserAction:fetch');
$app->get('/api/users/{id}', 'App\Action\UserAction:fetchOne');
