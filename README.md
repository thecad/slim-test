# Slim 3 met doctrine 2 voorbeeld

Ik heb een heel simpel api applicatie gemaakt die laat zien hoe Slim 3 en doctrine 2 in elkaar zit.
De applicatie kan worden getest op slim.marco.codes, de aangemaakt route's zijn:

1. http://slim.marco.codes/api/users
2. http://slim.marco.codes/api/users/1

Mochten er vragen zijn, vraag ze gerust.

## Opmerkingen

Ik heb ook het Slim 3 skeleton framework gebruikt, dit is een hele kale slim 3 applicatie waar, grotendeels van de configuratie al is gedaan.
De documentatie hier over staat onder aan dit document bij het kopje `Slim 3 Skeleton`

Slim werkt wel via het MVC model, alleen worden er andere namen voor gebruikt.
Je kunt de Actions zien als Controllers en de Entities als Models.
Views heb ik er niet inzitten, hij laat gewoon de content uit de database als JSON zien.

De setup van de database zal ik als image onder aan de pagina zetten.

# Belangrijke bestanden
De belangrijke bestanden waar naar gekeken moet worden zijn

1. `app/dependencies.php` van regel 40 tot 61: Dit is de manier om doctrine aan te melden bij de standaard Dependencie Injector die gebruikt wordt door Slim
2. `app/settings.php` van regel 25 tot 42: Dit is de config die doctrine nodig heeft om te werken.
3. `app/routes.php` van regel 7 tot 8: Dit is redelijk duidelijk, een apparte file voor het maken van routes.
4. `app/src/Action/UserAction.php` volledig: Dit is de controller die de opgravraagde data via een resource ophaalt uit de database en deze op het scherm toont als json.
5. `app/src/Entity/User.php` volledig: Dit is de file die de relatie legt tussen object oriented php en de database, let hier op de comments die gebruikt worden
																			 binnen het bestand, dit geeft aan de ORM(doctrine) door hoe de relatie's daadwerkelijk liggen.
6. `app/src/Resource/UserResource.php` volledig: Dit is een tussen bestand wat aan gesproken word door de controller en die praat met de Entity, hiermee houd je het mooi OO gescheiden.
7. `app/src/AbstractResource.php` volledig: Dit een abstracte classe die all resources moeten hebben, waar de entity manager van doctrine aangemaakt word, zodat er gecommuniceerd kan worden.


[Slim 3 skeleton op github](https://github.com/akrabat/slim3-skeleton)

# Slim 3 Skeleton

This is a simple skeleton project for Slim 3 that includes Twig, Flash messages and Monolog.

## Create your project:

    $ composer create-project --no-interaction --stability=dev akrabat/slim3-skeleton my-app

### Run it:

1. `$ cd my-app`
2. `$ php -S 0.0.0.0:8888 -t public public/index.php`
3. Browse to http://localhost:8888

## Key directories

* `app`: Application code
* `app/src`: All class files within the `App` namespace
* `app/templates`: Twig template files
* `cache/twig`: Twig's Autocreated cache files
* `log`: Log files
* `public`: Webserver root
* `vendor`: Composer dependencies

## Key files

* `public/index.php`: Entry point to application
* `app/settings.php`: Configuration
* `app/dependencies.php`: Services for Pimple
* `app/middleware.php`: Application middleware
* `app/routes.php`: All application routes are here
* `app/src/Action/HomeAction.php`: Action class for the home page
* `app/templates/home.twig`: Twig template file for the home page



# Database

![Database afbeelding](https://puu.sh/ymJIO/c24325ee15.png, Database afbeelding)
